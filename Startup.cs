using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BlazorTestIdentity.Areas.Identity;
using BlazorTestIdentity.Data;
using AspNetCore.Identity.MongoDbCore.Extensions;
using BlazorTestIdentity.Areas.Identity.Data;
using AspNetCore.Identity.MongoDbCore.Infrastructure;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;

namespace BlazorTestIdentity
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<IdentityUser>>();
            services.AddSingleton<WeatherForecastService>();

            

            var mongoDbIdentityConfiguration = new MongoDbIdentityConfiguration
            {
                MongoDbSettings = new MongoDbSettings
                {
                    ConnectionString = Configuration.GetSection(nameof(DatabaseSettings)).GetSection("ConnectionString").Value,
                    DatabaseName = Configuration.GetSection(nameof(DatabaseSettings)).GetSection("DatabaseName").Value
                },
                IdentityOptionsAction = options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 5;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;

                    // Lockout settings
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                    options.Lockout.MaxFailedAccessAttempts = 10;

                    // ApplicationUser settings
                    options.User.RequireUniqueEmail = true;
                    options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@.-_";
                }
            };

            services.AddSingleton<IEmailSender, EmailSender>();

            services.ConfigureMongoDbIdentity<ApplicationUser, ApplicationRole, Guid>(mongoDbIdentityConfiguration)
                .AddSignInManager();

            var sprov = services.BuildServiceProvider();
            InitRoleUserSetup(sprov).Wait();
        }

        public async Task InitRoleUserSetup(IServiceProvider serviceProvider)
        {

            //var userManager = serviceProvider.GetService<UserManager<IdentityUser>>();
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetService<RoleManager<ApplicationRole>>();

            var roleCheck = await roleManager.RoleExistsAsync("Admin");
            if (!roleCheck)
            {
                //create the roles and send them to the database 
                await roleManager.CreateAsync(new ApplicationRole("Admin"));
            }

            //roleCheck = await roleManager.RoleExistsAsync("Customer");
            //if (!roleCheck) { await roleManager.CreateAsync(new ApplicationRole("Customer")); }

            //roleCheck = await roleManager.RoleExistsAsync("Manager");
            //if (!roleCheck) { await roleManager.CreateAsync(new ApplicationRole("Manager")); }

            roleCheck = await roleManager.RoleExistsAsync("Regular");
            if (!roleCheck) { await roleManager.CreateAsync(new ApplicationRole("Regular")); }

            string userName = "admin";
            string userMail = "admin@domain.ch";
            string userPWD = "admin";

            var user = new ApplicationUser { UserName = userName, Email = userMail };
            var result = await userManager.CreateAsync(user, userPWD);

            //Add default User to Role Admin    
            if (result.Succeeded)
            {
                await userManager.AddToRoleAsync(user, "Admin");
            }

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
